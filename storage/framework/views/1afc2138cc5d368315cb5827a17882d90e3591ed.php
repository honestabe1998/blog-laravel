<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<div class="container">
    <div id="fh5co-intro">
        <div class="row animate-box">
            <div class="col-md-8 col-md-offset-2 col-md-pull-2">
                <h2><?php echo e($post->title); ?></h2>
            </div>
        </div>
    </div>
    <div id="fh5co-portfolio">
        <div class="row">

            <div class=" image-content">
                <div class="image-item   animate-box" style="width: 100%">
                    <img src="<?php echo e(asset('storage/images/'.$post->image_url)); ?>" class="img-responsive" alt="Free HTML5 Bootstrap Template by FreeHTML5.co">
                </div>
            </div>
           
        </div>
        <div class="">
            <div class="detail" id="sticky_item">
                <div class="animate-box">
                    <h2></h2>
                    <span></span>
                    <p><?php echo $post->description; ?></p>
                   
                </div>
            </div>
        </div>
    </div>
</div><!-- END container-wrap -->




<div class="gototop js-top">
    <a href="#" class="js-gotop"><i class="icon-arrow-up2"></i></a>
</div>

<!-- jQuery -->
<script src="js/jquery.min.js"></script>
<!-- jQuery Easing -->
<script src="js/jquery.easing.1.3.js"></script>
<!-- Bootstrap -->
<script src="js/bootstrap.min.js"></script>
<!-- Waypoints -->
<script src="js/jquery.waypoints.min.js"></script>
<!-- Flexslider -->
<script src="js/jquery.flexslider-min.js"></script>
<!-- Sticky Kit -->
<script src="js/sticky-kit.min.js"></script>
<!-- Main -->
<script src="js/main.js"></script>
<?php /**PATH C:\OpenServer\domains\laravel-blog\resources\views/single.blade.php ENDPATH**/ ?>