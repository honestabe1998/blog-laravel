<nav class="fh5co-nav" role="navigation">
    <div class="container">
        <div class="top-menu">
            <div class="row">
                <div class="col-sm-2">
                    <div id="fh5co-logo"><a href="<?php echo e(route('home')); ?>">My Blog<span>.</span></a></div>
                </div>
                <div class="col-sm-10 text-right menu-1">
                    <ul>

                        <li><a href="<?php echo e(route('contactUs')); ?>">Contact Us</a></li>
                        <?php if(Route::has('login')): ?>
                        <li class="has-dropdown">
                            <?php if(auth()->guard()->check()): ?>
                                <a href="#" >Dashboard</a>
                                <ul class="dropdown">
                                    <?php if(auth()->check() && auth()->user()->hasRole('admin')): ?> 
                                    <li> <a href="<?php echo e(route('admin')); ?>">Admin Panel</a></li>
                                    <?php endif; ?>
                                    <form method="POST" action="<?php echo e(route('logout')); ?>">
                                        <?php echo csrf_field(); ?>
                                        <li>
                                            <a href="<?php echo e(route('logout')); ?>"     onclick="event.preventDefault();
                                            this.closest('form').submit();">Log Out</a>
                                        </li>
                                    
                                    </form>
                                </ul>
                            <?php else: ?>
                                <li><a href="<?php echo e(route('login')); ?>" >Log in</a></li>
        
                                <?php if(Route::has('register')): ?>
                                    <li><a href="<?php echo e(route('register')); ?>">Register</a></li>
                                <?php endif; ?>
                            <?php endif; ?>
                        </li>
                    <?php endif; ?>
                    </ul>
                </div>
            </div>
            
        </div>
    </div>
</nav><?php /**PATH C:\OpenServer\domains\laravel-blog\resources\views/layouts/navigation.blade.php ENDPATH**/ ?>