<?php echo $__env->make('admin.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<h1>Contacts</h1>
<table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Name</th>
        <th scope="col">Email</th>
        <th scope="col">Message</th>
        <th scope="col">Status</th>
        <th scope="col">Actions</th>
      </tr>
    </thead>
    <tbody>
<?php $__currentLoopData = $contacts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
<tr>
    <th scope="row"><?php echo e($item->id); ?></th>
   
    <td><?php echo e($item->name); ?></td>
    <td style=""><?php echo e($item->email); ?></td>
    <td><?php echo e($item->message); ?></td>
    <td>
    <?php if($item->status == 0): ?>
        <span class="badge bg-danger">Not Answered</span>
      <?php else: ?>    
        <span class="badge bg-success">Answered</span>
    <?php endif; ?>
    </td>
    <td>
        <?php if($item->status == 0): ?>
        <a class="btn btn-primary" href="<?php echo e(route('answerEmail', $item->id)); ?>" role="button">Answer</a>

        <?php else: ?>    
        <a href="#" class="btn btn-primary btn-lg disabled" role="button" aria-disabled="true">Answer</a>

    <?php endif; ?>
    </td>
  </tr>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</tbody><?php /**PATH C:\OpenServer\domains\laravel-blog\resources\views/admin/contacts/index.blade.php ENDPATH**/ ?>