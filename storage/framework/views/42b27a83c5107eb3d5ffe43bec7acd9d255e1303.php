<?php echo $__env->make('admin.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<h1>Answer To Email</h1>
<?php if(session()->has('msg')): ?>
    <div class="alert alert-success">
        <?php echo e(session()->get('msg')); ?>

    </div>
<?php endif; ?>
<form action="<?php echo e(route('sendEmail', $contact->id)); ?>" method="POST" style="margin-left: 30%">
    <?php echo csrf_field(); ?>
    <div class="mb-3 ">
        <label for="title" class="form-label align-self-center">Title</label>
        <input type="text" class="form-control w-50" name="title"  id="title" >
    </div>
    <div class="mb-3">
        <label for="text" class="form-label align-self-center">Body</label>
        <input type="text" class="form-control w-50" name="body"  id="body" >
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
    <?php /**PATH C:\OpenServer\domains\laravel-blog\resources\views/admin/contacts/answer.blade.php ENDPATH**/ ?>