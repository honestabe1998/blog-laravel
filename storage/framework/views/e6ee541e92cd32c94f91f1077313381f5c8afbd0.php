
    <body class="antialiased">
        <?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            <div class="fh5co-loader"></div>
	
            <div id="page">
          
            <div class="container">
                <div id="fh5co-intro">
                    <div class="row animate-box">
                        <div class="col-md-8 col-md-offset-2 col-md-pull-2">
                            <h2>My Very First Project</h2>
                        </div>
                    </div>
                </div>
                <aside id="fh5co-hero">
                    <div class="flexslider">
                        <ul class="slides">
                            <?php $__currentLoopData = $posts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            
                            <li style="background-image:  url(<?php echo e(asset('storage/images/'.$item->image_url)); ?>)">
                                <div class="overlay"></div>
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-md-6 col-xs-8 col-md-offset-1 slider-text">
                                            <div class="slider-text-inner">
                                                <h1><a href="<?php echo e(route('single', $item->id)); ?>"><?php echo e($item->title); ?></a></h1>
                                                 
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            </li>	   	
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                     
                        
                          </ul>
                      </div>
                </aside>
                <div id="fh5co-portfolio">
                    <div class="row nopadding">
                        <div class="col-md-6 padding-right">
                            <div class="row">
                                <?php $__currentLoopData = $chunks[0]; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <div class="col-md-12 animate-box">
                                    <a href="<?php echo e(route('single', $item->id)); ?>" class="portfolio-grid">
                                        <img src="<?php echo e(asset('storage/images/'.$item->image_url)); ?>" class="img-responsive" alt="Free HTML5 Bootstrap Template by FreeHTML5.co">
                                        <div class="desc">
                                            <h3><?php echo e($item->title); ?></h3>
                                            <span></span>
                                        </div>
                                    </a>
                                </div>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                
                            </div>
                        </div>
        
                        <div class="col-md-6 padding-left">
                            <div class="row">
                              <?php $__currentLoopData = $chunks[1]; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                              
                              <div class="col-md-12 animate-box">
                                <a href="<?php echo e(route('single', $item->id)); ?>" class="portfolio-grid">
                                      <img src="<?php echo e(asset('storage/images/'.$item->image_url)); ?>" class="img-responsive" alt="Free HTML5 Bootstrap Template by FreeHTML5.co">
                                      <div class="desc">
                                          <h3><?php echo e($item->title); ?></h3>
                                          <span></span>
                                      </div>
                                  </a>
                              </div>
                              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php echo $__env->make('layouts.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        

            </div>
        
            <div class="gototop js-top">
                <a href="#" class="js-gotop"><i class="fas fa-arrow-up"></i></i></a>
            </div>
            
            <!-- jQuery -->
            <script src="<?php echo e(asset('js/jquery.min.js')); ?>"></script>
            <!-- jQuery Easing -->
            <script src="<?php echo e(asset('js/jquery.easing.1.3.js')); ?>"></script>
            <!-- Bootstrap -->
            <script src="<?php echo e(asset('js/bootstrap.min.js')); ?>"></script>
            <!-- Waypoints -->
            <script src="<?php echo e(asset('js/jquery.waypoints.min.js')); ?>"></script>
            <!-- Flexslider -->
            <script src="<?php echo e(asset('js/jquery.flexslider-min.js')); ?>"></script>
            <!-- Sticky Kit -->
            <script src="<?php echo e(asset('js/sticky-kit.min.js')); ?>"></script>
            <!-- Main -->
            <script src="<?php echo e(asset('js/main.js')); ?>"></script>
            <script src="https://kit.fontawesome.com/d3f34d128e.js" crossorigin="anonymous"></script>

  
  
        </body>
</html>
<?php /**PATH C:\OpenServer\domains\laravel-blog\resources\views/welcome.blade.php ENDPATH**/ ?>