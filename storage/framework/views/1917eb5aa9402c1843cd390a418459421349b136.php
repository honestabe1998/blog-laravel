<?php echo $__env->make('admin.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<h1>Posts </h1>
<a href="<?php echo e(route('posts.create')); ?>" class="btn btn-outline-success  m-3">Create A Post</a>
 
<table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Title</th>
        <th scope="col">Description</th>
        <th scope="col">Author ID</th>
        <th scope="col">Actions</th>
      </tr>
    </thead>
    <tbody>
<?php $__currentLoopData = $posts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
<tr>
    <th scope="row"><?php echo e($item->id); ?></th>
   
    <td><?php echo e($item->title); ?></td>
    <td style=""><?php echo e(strip_tags($item->description)); ?></td>
    <td><?php echo e($item->author_id); ?></td>
    <td class="d-flex">
      <a class="btn btn-warning" style="    height: 37px;" href="<?php echo e(route('posts.edit', $item->id)); ?>" role="button">Edit</a>
      <form style="margin-left: 10px" action="<?php echo e(route('posts.destroy', $item->id)); ?>" method="POST">
        <?php echo csrf_field(); ?>
        <?php echo method_field("DELETE"); ?>
        <button type="submit" class="btn btn-danger">Delete</button>

      </form>
    </td>
  </tr>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody><?php /**PATH C:\OpenServer\domains\laravel-blog\resources\views/admin/posts/index.blade.php ENDPATH**/ ?>