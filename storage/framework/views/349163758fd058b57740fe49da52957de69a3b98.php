<?php echo $__env->make('admin.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<h1>Users</h1>
<a href="<?php echo e(route('users.create')); ?>" class="btn btn-outline-success  m-3">Create A User</a>
<table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Name</th>
        <th scope="col">Email</th>
        <th scope="col">User Role</th>
        <th scope="col">Actions</th>
      </tr>
    </thead>
    <tbody>
      <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
      <tr>
        <th scope="row"><?php echo e($item->id); ?></th>
       
        <td><?php echo e($item->name); ?></td>
        <td><?php echo e($item->email); ?></td>
        <td><?php echo e($item->roles->pluck('name')); ?></td>
        <td class="d-flex">
          <a class="btn btn-warning" style="    height: 37px;" href="<?php echo e(route('users.edit', $item->id)); ?>" role="button">Edit</a>
          <form style="margin-left: 10px" action="<?php echo e(route('users.destroy', $item->id)); ?>" method="POST">
            <?php echo csrf_field(); ?>
            <?php echo method_field("DELETE"); ?>
            <button type="submit" class="btn btn-danger">Delete</button>

          </form>
        </td>
      </tr>
       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
      
      
    </tbody>
  </table><?php /**PATH C:\OpenServer\domains\laravel-blog\resources\views/admin/users/users_all.blade.php ENDPATH**/ ?>