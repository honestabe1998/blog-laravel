<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\FbController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\UsersController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ContactController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [Controller::class, 'home'])->name('home');



Route::get('auth/facebook', [FbController::class, 'redirectToFacebook']);
Route::get('auth/facebook/callback', [FbController::class, 'facebookSignin']);

Route::group(['middleware' => ['role:admin']], function () {
    Route::get('admin', [AdminController::class , 'index'])->name('admin');
    Route::resource('admin/users', UsersController::class);
    Route::resource('admin/posts', PostController::class);
    Route::get('/contacts', [ContactController::class, 'GetAll'])->name('contactsAll');
    Route::get('/contact/{id}', [ContactController::class, 'answerEmail'])->name('answerEmail');
    Route::post('/answer_contact/{id}', [ContactController::class, 'sendEmail'])->name('sendEmail');
});

Route::post('/contact_create', [ContactController::class, 'add'])->name('addContact');
Route::get('contact_us/' , [Controller::class , 'contactUs'])->name('contactUs');

Route::get('post/{id}',  [PostController::class , 'single'])->name('single');
require __DIR__.'/auth.php';
