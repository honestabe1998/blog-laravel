<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use App\Models\Post;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function home(){
        $posts = Post::paginate(15);
       
        $half = ceil($posts->count() / 2);
        $chunks = $posts->chunk($half);
        
        return view('welcome')->with(['chunks' => $chunks , 'posts'=>$posts]);
    }


    public function contactUs(){

        return view('pages.contact');
    }
}
