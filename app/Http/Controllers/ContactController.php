<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Contact;
use Illuminate\Support\Facades\Mail;

class ContactController extends Controller
{
    public function GetAll(){
        $contacts = Contact::paginate(15);
   

        return view('admin.contacts.index')->with('contacts', $contacts);
    }
    public function add(Request $request){
        $data = $request->all();
        $data['status'] = 0;
        $contact = Contact::create($data);

        return redirect()->back()->with('msg', 'We Will Answer You ASAP');
    }
    public function answerEmail($id){
        $contact = Contact::find($id);
        
        return view('admin.contacts.answer')->with('contact', $contact);
    }

    public function sendEmail(Request $request, $id){
        $contact = Contact::find($id);
        $details = [
            'title' => $request->title,
            'body' =>  $request->body,
        ];
        
        Mail::to($contact->email)->send(new \App\Mail\MyTestMail($details));
        $contact->status = 1;
        $contact->save();

        return redirect()->back()->with('msg', 'Email Sent');
    }
}
