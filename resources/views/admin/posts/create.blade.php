@include('admin.layouts.app')

<h1 class="text-center">Add Post</h1>

@if(session()->has('msg'))
    <div class="alert alert-success">
        {{ session()->get('msg') }}
    </div>
@endif
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<form style="margin-left: 30%" action="{{route('posts.store')}}" method="POST" enctype="multipart/form-data" >
    {{ csrf_field() }} 
    
  
    <div class="mb-3">
          <label for="title" class="form-label align-self-center">Title</label>
          <input type="text" class="form-control w-50" name="title"  id="title" >
        
        </div>
        <div class="mb-3">
            <label for="image">Choose Image</label>
            <input id="image" type="file" name="image_url">
          
          </div>
      <div class="mb-3">
        <label  for="description" class="form-label align-self-center">Description</label>
        <textarea  type="text" class="form-control w-50" name="description"  id="ckeditor" ></textarea>
      </div>
     
    
       <button type="submit" class="btn btn-primary">Submit</button>
    </form>

    <script type="text/javascript">
        $(document).ready(function () {
           var editor =  CKEDITOR.replace( 'ckeditor' );
    
        });
    </script>