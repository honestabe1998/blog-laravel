@include('admin.layouts.app')
<h1>Posts </h1>
<a href="{{route('posts.create')}}" class="btn btn-outline-success  m-3">Create A Post</a>
 
<table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Title</th>
        <th scope="col">Description</th>
        <th scope="col">Author ID</th>
        <th scope="col">Actions</th>
      </tr>
    </thead>
    <tbody>
@foreach ($posts as $item)
<tr>
    <th scope="row">{{$item->id}}</th>
   
    <td>{{$item->title}}</td>
    <td style="">{{ strip_tags($item->description) }}</td>
    <td>{{$item->author_id}}</td>
    <td class="d-flex">
      <a class="btn btn-warning" style="    height: 37px;" href="{{route('posts.edit', $item->id)}}" role="button">Edit</a>
      <form style="margin-left: 10px" action="{{route('posts.destroy', $item->id)}}" method="POST">
        @csrf
        @method("DELETE")
        <button type="submit" class="btn btn-danger">Delete</button>

      </form>
    </td>
  </tr>
@endforeach
    </tbody>