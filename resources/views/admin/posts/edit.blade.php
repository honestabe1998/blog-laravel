@include('admin.layouts.app')

<h1 class="text-center">Edit Post</h1>

@if(session()->has('msg'))
    <div class="alert alert-success">
        {{ session()->get('msg') }}
    </div>
@endif
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<form style="margin-left: 30%" action="{{route('posts.update', $post->id)}}" method="POST">
    @csrf
    @method('PUT')
  
    <div class="mb-3">
          <label for="title" class="form-label align-self-center">Title</label>
          <input type="text" class="form-control w-50" value="{{$post->title}}" name="title"  id="title" >
        
        </div>
      <div class="mb-3">
        <label  for="description" class="form-label align-self-center">Description</label>
        <textarea  type="text" class="form-control w-50" name="description"  id="ckeditor">{{$post->description}}</textarea>
      </div>
     
    
       <button type="submit" class="btn btn-primary">Submit</button>
    </form>

    <script type="text/javascript">
        $(document).ready(function () {
           var editor =  CKEDITOR.replace( 'ckeditor' );
    
        });
    </script>
