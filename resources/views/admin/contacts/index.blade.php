@include('admin.layouts.app')
<h1>Contacts</h1>
<table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Name</th>
        <th scope="col">Email</th>
        <th scope="col">Message</th>
        <th scope="col">Status</th>
        <th scope="col">Actions</th>
      </tr>
    </thead>
    <tbody>
@foreach ($contacts as $item)
<tr>
    <th scope="row">{{$item->id}}</th>
   
    <td>{{$item->name}}</td>
    <td style="">{{ $item->email }}</td>
    <td>{{$item->message}}</td>
    <td>
    @if ($item->status == 0)
        <span class="badge bg-danger">Not Answered</span>
      @else    
        <span class="badge bg-success">Answered</span>
    @endif
    </td>
    <td>
        @if ($item->status == 0)
        <a class="btn btn-primary" href="{{route('answerEmail', $item->id)}}" role="button">Answer</a>

        @else    
        <a href="#" class="btn btn-primary btn-lg disabled" role="button" aria-disabled="true">Answer</a>

    @endif
    </td>
  </tr>
@endforeach
</tbody>