@include('admin.layouts.app')

<h1>Answer To Email</h1>
@if(session()->has('msg'))
    <div class="alert alert-success">
        {{ session()->get('msg') }}
    </div>
@endif
<form action="{{route('sendEmail', $contact->id)}}" method="POST" style="margin-left: 30%">
    @csrf
    <div class="mb-3 ">
        <label for="title" class="form-label align-self-center">Title</label>
        <input type="text" class="form-control w-50" name="title"  id="title" >
    </div>
    <div class="mb-3">
        <label for="text" class="form-label align-self-center">Body</label>
        <input type="text" class="form-control w-50" name="body"  id="body" >
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
    