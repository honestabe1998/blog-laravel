@include('admin.layouts.app')
<h1>Users</h1>
<a href="{{route('users.create')}}" class="btn btn-outline-success  m-3">Create A User</a>
<table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Name</th>
        <th scope="col">Email</th>
        <th scope="col">User Role</th>
        <th scope="col">Actions</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($users as $item)
      <tr>
        <th scope="row">{{$item->id}}</th>
       
        <td>{{$item->name}}</td>
        <td>{{$item->email}}</td>
        <td>{{$item->roles->pluck('name')}}</td>
        <td class="d-flex">
          <a class="btn btn-warning" style="    height: 37px;" href="{{route('users.edit', $item->id)}}" role="button">Edit</a>
          <form style="margin-left: 10px" action="{{ route('users.destroy', $item->id)}}" method="POST">
            @csrf
            @method("DELETE")
            <button type="submit" class="btn btn-danger">Delete</button>

          </form>
        </td>
      </tr>
       @endforeach
      
      
    </tbody>
  </table>