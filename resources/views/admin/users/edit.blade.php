@include('admin.layouts.app')
<h1 class="text-center">Edit User</h1>
@if(session()->has('msg'))
    <div class="alert alert-success">
        {{ session()->get('msg') }}
    </div>
@endif
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<form style="margin-left: 30%" action="{{route('users.update', $user->id)}}" method="POST">
  @csrf
  @method('PUT')

  <div class="mb-3">
        <label for="name" class="form-label align-self-center">Name</label>
        <input type="name" class="form-control w-50" name="name" value="{{$user->name}}" id="name" >
      
      </div>
    <div class="mb-3">
      <label for="email" class="form-label align-self-center">Email address</label>
      <input type="email" class="form-control w-50" name="email" value="{{$user->email}}" id="email" >
    </div>
    
    <label for="role" class="form-label align-self-center">Role</label>
    <select class="form-select  w-50 mb-2" name="role"  id="role"aria-label="Default select example">
       
        @foreach ($roles as $role)
        <option value="{{$role->name}}">{{$role->name}}</option>
       @endforeach
      
    </select>
     <button type="submit" class="btn btn-primary">Submit</button>
  </form>