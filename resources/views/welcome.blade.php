
    <body class="antialiased">
        @include('layouts.app')
            <div class="fh5co-loader"></div>
	
            <div id="page">
          
            <div class="container">
                <div id="fh5co-intro">
                    <div class="row animate-box">
                        <div class="col-md-8 col-md-offset-2 col-md-pull-2">
                            <h2>My Very First Project</h2>
                        </div>
                    </div>
                </div>
                <aside id="fh5co-hero">
                    <div class="flexslider">
                        <ul class="slides">
                            @foreach ($posts as $item)
                            
                            <li style="background-image:  url({{ asset('storage/images/'.$item->image_url) }})">
                                <div class="overlay"></div>
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-md-6 col-xs-8 col-md-offset-1 slider-text">
                                            <div class="slider-text-inner">
                                                <h1><a href="{{route('single', $item->id)}}">{{$item->title}}</a></h1>
                                                 {{-- <h2>Free html5 templates Made by <a href="http://freehtml5.co/" target="_blank">freehtml5.co</a></h2> --}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            </li>	   	
                            @endforeach
                     
                        
                          </ul>
                      </div>
                </aside>
                <div id="fh5co-portfolio">
                    <div class="row nopadding">
                        <div class="col-md-6 padding-right">
                            <div class="row">
                                @foreach ($chunks[0] as $item)
                                <div class="col-md-12 animate-box">
                                    <a href="{{route('single', $item->id)}}" class="portfolio-grid">
                                        <img src="{{ asset('storage/images/'.$item->image_url) }}" class="img-responsive" alt="Free HTML5 Bootstrap Template by FreeHTML5.co">
                                        <div class="desc">
                                            <h3>{{$item->title}}</h3>
                                            <span></span>
                                        </div>
                                    </a>
                                </div>
                                @endforeach
                                
                            </div>
                        </div>
        
                        <div class="col-md-6 padding-left">
                            <div class="row">
                              @foreach ($chunks[1] as $item)
                              
                              <div class="col-md-12 animate-box">
                                <a href="{{route('single', $item->id)}}" class="portfolio-grid">
                                      <img src="{{ asset('storage/images/'.$item->image_url) }}" class="img-responsive" alt="Free HTML5 Bootstrap Template by FreeHTML5.co">
                                      <div class="desc">
                                          <h3>{{$item->title}}</h3>
                                          <span></span>
                                      </div>
                                  </a>
                              </div>
                              @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @include('layouts.footer')
        

            </div>
        
            <div class="gototop js-top">
                <a href="#" class="js-gotop"><i class="fas fa-arrow-up"></i></i></a>
            </div>
            
            <!-- jQuery -->
            <script src="{{asset('js/jquery.min.js')}}"></script>
            <!-- jQuery Easing -->
            <script src="{{asset('js/jquery.easing.1.3.js')}}"></script>
            <!-- Bootstrap -->
            <script src="{{asset('js/bootstrap.min.js')}}"></script>
            <!-- Waypoints -->
            <script src="{{asset('js/jquery.waypoints.min.js')}}"></script>
            <!-- Flexslider -->
            <script src="{{asset('js/jquery.flexslider-min.js')}}"></script>
            <!-- Sticky Kit -->
            <script src="{{asset('js/sticky-kit.min.js')}}"></script>
            <!-- Main -->
            <script src="{{asset('js/main.js')}}"></script>
            <script src="https://kit.fontawesome.com/d3f34d128e.js" crossorigin="anonymous"></script>

  
  
        </body>
</html>
