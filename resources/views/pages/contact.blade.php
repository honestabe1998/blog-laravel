@include('layouts.app')
<div class="container">
    <div id="fh5co-intro">
        <div class="row animate-box">
            <div class="col-md-8 col-md-offset-2 col-md-pull-2">
                <h2>Contact Us</h2>
            </div>
        </div>
    </div>
    <div id="fh5co-contact">
        <div class="row">
            @if(session()->has('msg'))
                <div class="alert alert-success">
                    {{ session()->get('msg') }}
                </div>
            @endif
            <div class="col-md-4 animate-box">
                <h3>Contact Information</h3>
                <ul class="contact-info">
                    <li><i class="fas fa-address-card"></i>Shinararneri 2 5</li>
                    <li><i class="fas fa-phone"></i>+ 37477005867</li>
                    <li><i class="fas fa-envelope-square"></i><a href="#">abrahamkarapetyan1998@gmail.com</a></li>

                </ul>
            </div>
            <form action="{{route('addContact')}}" method="POST">
                @csrf
            <div class="col-md-8 animate-box">
                <div class="form-wrap">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <input type="text" class="form-control" name="name" placeholder="Name">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <input type="text" class="form-control" name="email" placeholder="Email">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <textarea name="message" class="form-control" id="" cols="30"  rows="15" placeholder="Message"></textarea>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <input type="submit" value="Send Message" class="btn btn-primary btn-modify">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        </div>
    </div>
</div><!-- END container -->
 @include('layouts.footer')

</div>

<div class="gototop js-top">
    <a href="#" class="js-gotop"><i class="fas fa-arrow-up"></i></a>
</div>