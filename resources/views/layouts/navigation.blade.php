<nav class="fh5co-nav" role="navigation">
    <div class="container">
        <div class="top-menu">
            <div class="row">
                <div class="col-sm-2">
                    <div id="fh5co-logo"><a href="{{route('home')}}">My Blog<span>.</span></a></div>
                </div>
                <div class="col-sm-10 text-right menu-1">
                    <ul>

                        <li><a href="{{route('contactUs')}}">Contact Us</a></li>
                        @if (Route::has('login'))
                        <li class="has-dropdown">
                            @auth
                                <a href="#" >Dashboard</a>
                                <ul class="dropdown">
                                    @hasrole('admin') 
                                    <li> <a href="{{route('admin')}}">Admin Panel</a></li>
                                    @endhasrole
                                    <form method="POST" action="{{ route('logout') }}">
                                        @csrf
                                        <li>
                                            <a href="{{route('logout')}}"     onclick="event.preventDefault();
                                            this.closest('form').submit();">Log Out</a>
                                        </li>
                                    
                                    </form>
                                </ul>
                            @else
                                <li><a href="{{ route('login') }}" >Log in</a></li>
        
                                @if (Route::has('register'))
                                    <li><a href="{{ route('register') }}">Register</a></li>
                                @endif
                            @endauth
                        </li>
                    @endif
                    </ul>
                </div>
            </div>
            
        </div>
    </div>
</nav>