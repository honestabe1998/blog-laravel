<div class="container">
    <footer id="fh5co-footer" role="contentinfo">
        <div class="row">
            <div class="col-md-3 fh5co-widget">
                <h4>About Me</h4>
                <p>Web Developer from Armenia, I love this game ap karochi haves chka</p>
            </div>
            <div class="col-md-3 col-md-push-1">
                <h4>Latest Projects</h4>
                <ul class="fh5co-footer-links">
                    <li><a href="#"></a></li>
                </ul>
            </div>

            <div class="col-md-3 col-md-push-1">
                <h4>Links</h4>
                <ul class="fh5co-footer-links">
                <li><a href="{{route('home')}}">Home</a></li>
                    <li><a href="#">Work</a></li>
                    <li><a href="#">Services</a></li>
                    <li><a href="#">Blog</a></li>
                    <li><a href="#">About us</a></li>
                </ul>
            </div>

            <div class="col-md-3">
                <h4>Contact Information</h4>
                <ul class="fh5co-footer-links">
                    <li>Gyumri , Armenia <br> Shinararners street 2/5 </li>
                    <li><a href="tel://1234567920">+37477005867</a></li>
                    <li><a href="mailto:info@yoursite.com">abrahamkarapetyan1998@gmail.com</a></li>
                  
                </ul>
            </div>

        </div>

        <div class="row copyright">
            <div class="col-md-12 text-center">
                <p>
                    <small class="block">&copy; 2016 Free HTML5. All Rights Reserved.</small> 
                    <small class="block">Designed by <a href="http://freehtml5.co/" target="_blank">FreeHTML5.co</a> Demo Images: <a href="http://unsplash.co/" target="_blank">Unsplash</a></small>
                </p>
                <p>
                    <ul class="fh5co-social-icons">
                        <li><a href="https://www.facebook.com/profile.php?id=100012050408881" target="_blank"><i class="fab fa-facebook"></i></a></li>
                        <li><a href="https://www.instagram.com/abrahamkarapetyan1/" target="_blank"><i class="fab fa-instagram"></i></a></li>
                        <li><a href="https://www.linkedin.com/in/abraham-karapetyan-972321167/" target="_blank"><i class="fab fa-linkedin"></i></a></li>
                        <li><a href="https://bitbucket.org/honestabe1998/" target="_blank"><i class="fab fa-bitbucket"></i></a></li>
                    </ul>
                </p>
            </div>
        </div>
    </footer>
</div>